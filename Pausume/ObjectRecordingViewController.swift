//
//  ObjectRecordingViewController.swift
//  Pausume
//
//  Created by DJ Kim on 18/07/2019.
//  Copyright © 2019 DJKim. All rights reserved.
//

import UIKit
import Vision
import CoreMedia

class ObjectRecordingViewController: UIViewController, PreviewViewDelegate {
    
    var request: VNCoreMLRequest?
    var visionModel: VNCoreMLModel?
    
    // MARK: - AV Property
    let semaphore = DispatchSemaphore(value: 1)
    var lastExecution = Date()
    private let measure = Measure()
    typealias EstimationModel = MobileNetV2_SSDLite
    
    // MARK: - TableView Data
    var predictions: [VNRecognizedObjectObservation] = []

    var resumeBarButton: UIBarButtonItem?
    var pauseBarButton: UIBarButtonItem?
    var doneBarButton: UIBarButtonItem?
    
    var previewView: PreviewView?
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        view.backgroundColor = .black
        
        resumeBarButton = UIBarButtonItem(title: "resume".capitalized, style: .plain, target: self, action: #selector(resumeButtonPressed))
        pauseBarButton = UIBarButtonItem(title: "pause".capitalized, style: .plain, target: self, action: #selector(pauseButtonPressed))
        doneBarButton = UIBarButtonItem(title: "done".capitalized, style: .plain, target: self, action: #selector(doneButtonPressed))
        navigationItem.rightBarButtonItems = [doneBarButton!, pauseBarButton!, resumeBarButton!]
        /*
        resumeBarButton?.isEnabled = true
        resumeBarButton?.title = "record".capitalized
        pauseBarButton?.isEnabled = false
        doneBarButton?.isEnabled = false*/
        
        setUpModel()
        
        setupPreviewView()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        previewView?.beginSession()
    }
    
    func setupPreviewView() {
        print("setup preview")
        if previewView == nil {
            previewView = PreviewView()
            previewView?.delegate = self
            previewView?.backgroundColor = .black
            previewView?.frame = view.bounds
            view.addSubview(previewView!)
        }
    }
    
    
    // MARK: - Actions

    @objc func resumeButtonPressed() {
        print("resume")
        previewView?.startRecording()
        
        resumeBarButton?.isEnabled = false
        pauseBarButton?.isEnabled = true
        doneBarButton?.isEnabled = false
    }
    
    @objc func pauseButtonPressed() {
        print("pause")
        previewView?.stopRecording()
        resumeBarButton?.isEnabled = true
        resumeBarButton?.title = "resume".capitalized
        pauseBarButton?.isEnabled = false
        doneBarButton?.isEnabled = true
    }
    
    @objc func doneButtonPressed() {
        print("done")
        previewView?.mergeVideos()
    }
    
    func setUpModel() {
        if let visionModel = try? VNCoreMLModel(for: EstimationModel().model) {
            self.visionModel = visionModel
            request = VNCoreMLRequest(model: visionModel, completionHandler: visionRequestDidComplete)
            request?.imageCropAndScaleOption = .scaleFill
        } else {
            fatalError()
        }
    }
    
    func previewViewVideoSaved(_ previewView: PreviewView) {
        let alert = UIAlertController(title: "Saved!", message: "Video saved!", preferredStyle: .alert)
        let action = UIAlertAction(title: "OK", style: .cancel) { (action) in
            self.navigationController?.popViewController(animated: true)
        }
        alert.addAction(action)

        present(alert, animated: true, completion: nil)
    }
    
    func previewView(_ previewView: PreviewView, didCaptureVideoFrame pixelBuffer: CVPixelBuffer?, timestamp: CMTime) {
        if let pixelBuffer = pixelBuffer {
            // start of measure
            self.measure.start()
            
            // predict!
            self.predictUsingVision(pixelBuffer: pixelBuffer)
        }
    }
    
    func predictUsingVision(pixelBuffer: CVPixelBuffer) {
        //print("predictUsingVision")
        guard let request = request else { fatalError() }
        // vision framework configures the input size of image following our model's input configuration automatically
        self.semaphore.wait()
        let handler = VNImageRequestHandler(cvPixelBuffer: pixelBuffer)
        try? handler.perform([request])
    }
    
    // MARK: - Poseprocessing
    func visionRequestDidComplete(request: VNRequest, error: Error?) {

        self.measure.label(with: "endInference")
        if let predictions = request.results as? [VNRecognizedObjectObservation] {
            //            print(predictions.first?.labels.first?.identifier ?? "nil")
            //            print(predictions.first?.labels.first?.confidence ?? -1)
            for prediction in predictions {
                if let objectName = prediction.labels.first?.identifier {
                    if objectName == "cup" && !self.previewView!.didStartRecording {
                        print("found cup!")
                        DispatchQueue.main.async {
                            self.resumeButtonPressed()
                        }
                        
                        DispatchQueue.main.asyncAfter(deadline: .now() + 2, execute: {
                            self.pauseButtonPressed()
                        })
                        
                        
                    }
                }
            }
            self.predictions = predictions
            self.measure.stop()
        } else {
            // end of measure
            self.measure.stop()
        }
        self.semaphore.signal()
    }

}
