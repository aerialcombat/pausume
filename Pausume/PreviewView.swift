//
//  PreviewView.swift
//  Pausume
//
//  Created by DJ Kim on 18/07/2019.
//  Copyright © 2019 DJKim. All rights reserved.
//

import UIKit
import AVFoundation
import Photos

protocol PreviewViewDelegate {
    func previewViewVideoSaved(_ previewView: PreviewView)
    func previewView(_ previewView: PreviewView, didCaptureVideoFrame: CVPixelBuffer?, timestamp: CMTime)
}

class PreviewView: UIView, AVCaptureFileOutputRecordingDelegate, AVCaptureVideoDataOutputSampleBufferDelegate {
    
    var delegate: PreviewViewDelegate?
    
    private lazy var recordingSession: AVCaptureSession = {
        let _session = AVCaptureSession()
        _session.sessionPreset = .vga640x480
        return _session
    }()
    
    private lazy var mlSession: AVCaptureSession = {
        let _session = AVCaptureSession()
        _session.sessionPreset = .vga640x480
        return _session
    }()
    
    private lazy var recordingPreviewLayer: AVCaptureVideoPreviewLayer = {
        let _previewLayer = AVCaptureVideoPreviewLayer(session: recordingSession)
        _previewLayer.videoGravity = .resizeAspectFill
        return _previewLayer
    }()
    
    private lazy var mlPreviewLayer: AVCaptureVideoPreviewLayer = {
        let _previewLayer = AVCaptureVideoPreviewLayer(session: mlSession)
        _previewLayer.videoGravity = .resizeAspectFill
        return _previewLayer
    }()
    
    private let camera: AVCaptureDevice? = AVCaptureDevice.default(.builtInWideAngleCamera, for: .video, position: .back)
    
    private let camera2: AVCaptureDevice? = AVCaptureDevice.default(.builtInWideAngleCamera, for: .video, position: .back)
    
    private let microphone: AVCaptureDevice? = AVCaptureDevice.default(for: AVMediaType.audio)
    private let movieOutput = AVCaptureMovieFileOutput()
    private let videoDataOutput = AVCaptureVideoDataOutput()
    
    var didStartRecording: Bool = false
    
    var mainCompositionWidth: CGFloat = UIScreen.main.bounds.width
    var mainCompositionHeight: CGFloat = UIScreen.main.bounds.height
    let queue = DispatchQueue(label: "com.djkim.object-detection-queue")
    var activeInput: AVCaptureDeviceInput!
    var lastTimestamp = CMTime()
    
    var videoPathUrls: [URL] = []
    
    func stopSession() {
        if recordingSession.isRunning {
            videoQueue().async {
                self.recordingSession.stopRunning()
            }
        }
    }
    
    func videoQueue() -> DispatchQueue {
        return DispatchQueue.main
    }
    
    func currentVideoOrientation() -> AVCaptureVideoOrientation {
        var orientation: AVCaptureVideoOrientation
        
        switch UIDevice.current.orientation {
        case .portrait:
            orientation = AVCaptureVideoOrientation.portrait
        case .landscapeRight:
            orientation = AVCaptureVideoOrientation.landscapeLeft
        case .portraitUpsideDown:
            orientation = AVCaptureVideoOrientation.portraitUpsideDown
        default:
            orientation = AVCaptureVideoOrientation.landscapeRight
        }
        
        return orientation
    }
    
    func beginSession() {
        print("begin session")
        do {
            guard let _camera = camera else {
                fatalError("Camera doesn't work on the simulator! You have to test this on an actual device!")
            }

            // Video
            let deviceInput = try AVCaptureDeviceInput(device: _camera)
            let deviceInput2 = try AVCaptureDeviceInput(device: camera2!)
            if self.recordingSession.canAddInput(deviceInput) {
                self.recordingSession.addInput(deviceInput)
                activeInput = deviceInput
            }
            
            if self.mlSession.canAddInput(deviceInput2) {
                self.mlSession.addInput(deviceInput2)
            }
        
            // Video output
            
            if recordingSession.canAddOutput(movieOutput) {
                recordingSession.addOutput(movieOutput)
            }
            
            if mlSession.canAddOutput(videoDataOutput) {
                mlSession.addOutput(videoDataOutput)
                // Add a video data output
                videoDataOutput.alwaysDiscardsLateVideoFrames = true
                videoDataOutput.videoSettings = [kCVPixelBufferPixelFormatTypeKey as String: Int(kCVPixelFormatType_420YpCbCr8BiPlanarFullRange)]
                videoDataOutput.setSampleBufferDelegate(self, queue: queue)
            } else {
                print("Could not add video data output to the session")
                mlSession.commitConfiguration()
                return
            }

            DispatchQueue.main.async {
                self.recordingPreviewLayer.frame = self.bounds
                self.mlPreviewLayer.frame = self.bounds
                self.layer.masksToBounds = true
                self.layer.addSublayer(self.recordingPreviewLayer)
                self.layer.addSublayer(self.mlPreviewLayer)
                //self.recordingSession.startRunning()
                self.mlSession.startRunning()
                
                self.recordingPreviewLayer.isHidden = true

            }
            
            recordingSession.commitConfiguration()
            mlSession.commitConfiguration()
            
            
        } catch let error {
            debugPrint("\(self.self): \(#function) line: \(#line).  \(error.localizedDescription)")
        }
    }
    
    func startRecording() {
        didStartRecording = true
        
        if !movieOutput.isRecording {
            
            self.recordingSession.startRunning()
            self.mlSession.stopRunning()
            recordingPreviewLayer.isHidden = false
            mlPreviewLayer.isHidden = true
            
            let connection = movieOutput.connection(with: AVMediaType.video)
            
            if (connection?.isVideoOrientationSupported)! {
                connection?.videoOrientation = currentVideoOrientation()
            }
            
            if (connection?.isVideoStabilizationSupported)! {
                connection?.preferredVideoStabilizationMode = AVCaptureVideoStabilizationMode.auto
            }
            
            let device = activeInput.device
            
            if (device.isSmoothAutoFocusSupported) {
                
                do {
                    try device.lockForConfiguration()
                    device.isSmoothAutoFocusEnabled = false
                    device.unlockForConfiguration()
                } catch {
                    print("Error setting configuration: \(error)")
                }
                
            }

            movieOutput.startRecording(to: tempURL()!, recordingDelegate: self)
        }
        else {
            //stopRecording()
        }
        
    }
    
    func stopRecording() {
        
        if movieOutput.isRecording {
            movieOutput.stopRecording()
            
            recordingSession.stopRunning()
            mlSession.startRunning()
            recordingPreviewLayer.isHidden = true
            mlPreviewLayer.isHidden = false
            
            didStartRecording = false
        }
    }
    
    func tempURL() -> URL? {
        let directory = NSTemporaryDirectory() as NSString
        
        if directory != "" {
            let path = directory.appendingPathComponent(NSUUID().uuidString + ".mp4")
            return URL(fileURLWithPath: path)
        }
        
        return nil
    }
    
    func fileOutput(_ output: AVCaptureFileOutput, didStartRecordingTo fileURL: URL, from connections: [AVCaptureConnection]) {
        print("didStartRecordingTo: \(fileURL)")
    }
    
    func fileOutput(_ output: AVCaptureFileOutput, didFinishRecordingTo outputFileURL: URL, from connections: [AVCaptureConnection], error: Error?) {
        print("didFinishRecordingTo: \(outputFileURL)")
        
        videoPathUrls.append(outputFileURL)
    }
    
    func mergeVideos() {
        
        print("1")
        let composition = AVMutableComposition()
        var layerInstructions: [AVMutableVideoCompositionLayerInstruction] = []
        var duration: CMTime = .zero
        for url in videoPathUrls {
            print("2")
            let asset = AVAsset(url: url)
            print(asset.tracks(withMediaType: .video))
            guard let track = composition.addMutableTrack(withMediaType: .video,
                                                                  preferredTrackID: Int32(kCMPersistentTrackID_Invalid)) else { return }
            do {
                try track.insertTimeRange(CMTimeRangeMake(start: .zero, duration:asset.duration),
                                               of: asset.tracks(withMediaType: .video)[0],
                                               at: duration)
            } catch {
                print("Failed to load track")
                return
            }
            
            duration = CMTimeAdd(duration, asset.duration)
            let instruction = videoCompositionInstruction(track, asset: asset)
            instruction.setOpacity(0.0, at: duration)
            layerInstructions.append(instruction)
        }
        print("1")
        let mainInstruction = AVMutableVideoCompositionInstruction()
        mainInstruction.timeRange = CMTimeRangeMake(start: .zero, duration: duration)
        mainInstruction.layerInstructions = layerInstructions
        
 
        let mainComposition = AVMutableVideoComposition()
        mainComposition.instructions = [mainInstruction]
        mainComposition.frameDuration = CMTimeMake(value: 1, timescale: 30)
        
        
        
        if self.mainCompositionWidth.truncatingRemainder(dividingBy: 16) > 0 { // find the right resolution that can be divided by 16
            self.mainCompositionWidth = self.mainCompositionWidth + 1.0
        }
        
        if self.mainCompositionHeight.truncatingRemainder(dividingBy: 16) > 0 { // find the right resolution that can be divided by 16
            self.mainCompositionHeight = self.mainCompositionHeight + 1.0
        }
        
        mainComposition.renderSize = CGSize(width: mainCompositionWidth, height: mainCompositionHeight)
print("1")
        guard let documentDirectory = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).first else { return }
        let dateFormatter = DateFormatter()
        dateFormatter.dateStyle = .long
        dateFormatter.timeStyle = .short
        let random = arc4random() % 99999
        let url = documentDirectory.appendingPathComponent("mergeVideo-\(random).mp4")
  
        guard let exportSession = AVAssetExportSession(asset: composition, presetName: AVAssetExportPresetHighestQuality) else { return }
        exportSession.outputURL = url
        exportSession.outputFileType = .mp4
        exportSession.shouldOptimizeForNetworkUse = true
        exportSession.videoComposition = mainComposition
        
        exportSession.exportAsynchronously() {
            self.exportDidFinish(exportSession)
        }
    }
    
    func orientationFromTransform(_ transform: CGAffineTransform) -> (orientation: UIImage.Orientation, isPortrait: Bool) {
        var assetOrientation = UIImage.Orientation.up
        var isPortrait = false
        if transform.a == 0 && transform.b == 1.0 && transform.c == -1.0 && transform.d == 0 {
            assetOrientation = .right
            isPortrait = true
        } else if transform.a == 0 && transform.b == -1.0 && transform.c == 1.0 && transform.d == 0 {
            assetOrientation = .left
            isPortrait = true
        } else if transform.a == 1.0 && transform.b == 0 && transform.c == 0 && transform.d == 1.0 {
            assetOrientation = .up
        } else if transform.a == -1.0 && transform.b == 0 && transform.c == 0 && transform.d == -1.0 {
            assetOrientation = .down
        }
        return (assetOrientation, isPortrait)
    }
    
    func videoCompositionInstruction(_ track: AVCompositionTrack, asset: AVAsset) -> AVMutableVideoCompositionLayerInstruction {
        let instruction = AVMutableVideoCompositionLayerInstruction(assetTrack: track)
        let assetTrack = asset.tracks(withMediaType: AVMediaType.video)[0]
        
        let transform = assetTrack.preferredTransform
        let assetInfo = orientationFromTransform(transform)
        
        var scaleToFitRatio = mainCompositionWidth / assetTrack.naturalSize.height
        if assetInfo.isPortrait {
            scaleToFitRatio = UIScreen.main.bounds.width / assetTrack.naturalSize.height
            let scaleFactor = CGAffineTransform(scaleX: scaleToFitRatio, y: scaleToFitRatio)
            instruction.setTransform(assetTrack.preferredTransform.concatenating(scaleFactor), at: .zero)
        } else {
            let scaleFactor = CGAffineTransform(scaleX: scaleToFitRatio, y: scaleToFitRatio)
            var concat = assetTrack.preferredTransform.concatenating(scaleFactor)
                .concatenating(CGAffineTransform(translationX: 0, y: UIScreen.main.bounds.width / 2))
            if assetInfo.orientation == .down {
                let fixUpsideDown = CGAffineTransform(rotationAngle: CGFloat(Double.pi))
                let windowBounds = UIScreen.main.bounds
                let yFix = assetTrack.naturalSize.height + windowBounds.height
                let centerFix = CGAffineTransform(translationX: assetTrack.naturalSize.width, y: yFix)
                concat = fixUpsideDown.concatenating(centerFix).concatenating(scaleFactor)
            }
            instruction.setTransform(concat, at: .zero)
        }
        
        return instruction
    }
    
    func exportDidFinish(_ exportSession: AVAssetExportSession) {
        if let outputURL = exportSession.outputURL {
            let saveVideoToPhotos = {
                PHPhotoLibrary.shared().performChanges({ PHAssetChangeRequest.creationRequestForAssetFromVideo(atFileURL: outputURL) }) { saved, error in
                    if saved {
                        self.delegate?.previewViewVideoSaved(self)
                    } else {
                        print("error: \(error?.localizedDescription ?? "")")
                    }
                }
            }
            
            // Ensure permission to access Photo Library
            if PHPhotoLibrary.authorizationStatus() != .authorized {
                PHPhotoLibrary.requestAuthorization({ status in
                    if status == .authorized {
                        saveVideoToPhotos()
                    }
                })
            } else {
                saveVideoToPhotos()
            }
        } else {
            print("no output url")
        }
    }
    
    public func captureOutput(_ output: AVCaptureOutput, didOutput sampleBuffer: CMSampleBuffer, from connection: AVCaptureConnection) {

        // Because lowering the capture device's FPS looks ugly in the preview,
        // we capture at full speed but only call the delegate at its desired
        // framerate.
        let timestamp = CMSampleBufferGetPresentationTimeStamp(sampleBuffer)
        let deltaTime = timestamp - lastTimestamp
        if deltaTime >= CMTimeMake(value: 1, timescale: Int32(15)) {
            lastTimestamp = timestamp
            let imageBuffer = CMSampleBufferGetImageBuffer(sampleBuffer)
            delegate?.previewView(self, didCaptureVideoFrame: imageBuffer, timestamp: timestamp)
        }
    }
    
    public func captureOutput(_ output: AVCaptureOutput, didDrop sampleBuffer: CMSampleBuffer, from connection: AVCaptureConnection) {

    }

}
