//
//  ViewController.swift
//  Pausume
//
//  Created by DJ Kim on 18/07/2019.
//  Copyright © 2019 DJKim. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
    }


    @IBAction func startVideoButtonPressed(_ sender: UIButton) {
        performSegue(withIdentifier: "showObjectRecordingView", sender: self)
    }
}

